<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNotificationsTable extends Migration {

	public function up()
	{
		Schema::create('notifications', function(Blueprint $table) {
			$table->id();
			$table->timestamps();
			$table->string('type');
			$table->string('notifiable');
			$table->string('data');
		});
	}

	public function down()
	{
		Schema::drop('notifications');
	}
}
