<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserCvTable extends Migration {

	public function up()
	{
		Schema::create('user_cv', function(Blueprint $table) {
			$table->id();
			$table->timestamps();
			$table->softDeletes();
			$table->string('job_state');
			$table->string('job_current');
			$table->string('image');
			$table->json('languages');
			$table->string('cv_file')->nullable();
			$table->biginteger('user_id')->unsigned();
			$table->enum('job_level', array(''));
			$table->enum('job_environment', array(''));
			$table->string('job_time');
			$table->string('job_field');
			$table->string('skills');
		});
	}

	public function down()
	{
		Schema::drop('user_cv');
	}
}
