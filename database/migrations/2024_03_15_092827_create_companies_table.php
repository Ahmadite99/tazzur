<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompaniesTable extends Migration {

	public function up()
	{
		Schema::create('companies', function(Blueprint $table) {
			$table->id();
			$table->timestamps();
			$table->softDeletes();
			$table->enum('status', array('waiting', 'acceptable', 'rejected', 'banned'));
			$table->string('name')->unique();
			$table->string('phone')->unique();
			$table->string('topic');
			$table->json('location');
			$table->string('fax');
			$table->json('documents');
			$table->string('type');
			$table->string('logo');
			$table->string('otp_code');
			$table->string('email');
			$table->string('email_verification');
			$table->string('about_us');
		});
	}

	public function down()
	{
		Schema::drop('companies');
	}
}
