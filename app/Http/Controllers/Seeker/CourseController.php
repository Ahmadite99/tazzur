<?php

namespace App\Http\Controllers\Seeker;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
       //parameters ? :   topic - address - key
       //response :
      //  name_compnay - image_company - start_date - days - duration - location - name_course - price
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        // - request :
        // token - id course
        // - response :
        // all data course with company(logo - name - about)
    }
    public function Interested(string $id)
    {
        // - request :
        // token - id course
        // - response :
        // true
    }

}
