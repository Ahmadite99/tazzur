<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('admin')->group(fn() => require __DIR__ . '/api/admin.php');
Route::prefix('advisor')->group(fn() => require __DIR__ . '/api/advisor.php');
Route::prefix('company')->group(fn() => require __DIR__ . '/api/company.php');
Route::prefix('seeker')->group(fn() => require __DIR__ . '/api/seeker.php');

